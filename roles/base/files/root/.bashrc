#
# Managed by ansible-server
#
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias update='apt update'
alias upgrade='apt upgrade'
alias dist-upgrade='apt-get dist-upgrade'

export LANG="en_ZA.UTF-8"
export LANGUAGE="en_ZA.UTF-8"
export LC_ALL="en_ZA.UTF-8"
export LC_CTYPE="en_ZA.UTF-8"

export TERM="xterm-256color"
PS1="[\d \A \u:\h] [\w] \\$\[$(tput sgr0)\] "
